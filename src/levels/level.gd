extends Node2D

var targets = preload("res://assets/objs/targets/target.tscn")
var gunSound = preload("res://assets/sounds/gunshot.tscn")
var target_level = 1

func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var targets_num = get_tree().get_nodes_in_group("targets").size()
	update_hud()

	if targets_num < target_level:
		#print("new_target")
		load_target()
		
func update_hud():
	$HUD/Label.text = "Score: " + str(Global.score)
	$HUD/Label.text += "\nLevel: " + str(Global.level)
	
func _input(event):
	if !event.is_pressed():
		return
	var key = event.as_text()
	if key == "F1":
		get_tree().paused = !get_tree().paused
		$help_menu.visible = get_tree().paused
		
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().change_scene("res://title_screen/main_title.tscn")
		
	if key.length() > 1:
		return
		
	play_sound()

func play_sound():
	var sound = gunSound.instance()
	add_child(sound)
	

func load_target():
	var target = targets.instance()
	
	var y = 400
	var width = 600
	randomize()
	var x = rand_range(0,width)
	
	target.transform = Transform2D(0.0, Vector2(x, y))
	add_child(target)



func _on_target_level_up_timeout():
	target_level += 1
	if target_level > 3:
		target_level = 3
