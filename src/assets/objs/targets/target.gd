extends RigidBody2D

export (int) var spin_thrust

var thrust = Vector2(10,-100)
var dir = 100
var spin = 0
var active = false
var death = preload("res://objects/explosion/explosion.tscn")

onready var label = $letters/Label

# Called when the node enters the scene tree for the first time.
func _ready():
	#position.y = 400
	var i = rand_range(0,Global.letterlist[Global.word_length].size())
	$letters/Label.text = Global.letterlist[Global.word_length][i]
	randomize()
	
	spin = rand_range(-20,20)

func _physics_process(delta):
	if !active:
		load_target()
	
	apply_torque_impulse(spin)
	
func load_target():
	active = true
	
	if position.x > 200:
		#print(position.x)
		dir = dir * -1
	
	
	var upward = rand_range(-400,-300)
	thrust = Vector2(dir,upward)
	apply_impulse(Vector2(0,0), thrust)

func _input(event):
	var key = event.as_text()
	if key.length() > 1:
		return
		
	if label.text.length() > 0:
		var letter = label.text[0]
		if event.is_pressed() && key == letter:
			Global.score += 25 + Global.level
			var text = label.text
			text.erase(0,1)
			label.text = text
			if label.text.length() == 0:
				kill()
		else:
			Global.score -= 5
	else:
		kill()
	
func kill():
	var explosion = death.instance()
	explosion.position = position
	get_tree().get_current_scene().add_child(explosion)
	queue_free()
	
func missed_target():
	if active:
		queue_free()


func _on_VisibilityNotifier2D_screen_entered():
	active = true

