extends Node

var letterlist = []
var score = 0
var level = 1

var word_length = 0
var word_length_max = 2 #is one more then this number
var music_bus = AudioServer.get_bus_index("music")


func _ready():
	Save.load()
	var list_1 = load_list("res://assets/wordlists/1.lst")
	var list_2 = load_list("res://assets/wordlists/2.lst")
	var list_3 = load_list("res://assets/wordlists/3.lst")
	var list_4 = load_list("res://assets/wordlists/4.lst")
	letterlist = [list_1,list_2,list_3,list_4]

	#print(letterlist[1][5])

func _process(delta):
	level = score/1000 + 1
	word_length = clamp(level-1,0,word_length_max)
	
func load_list(path):
	var f = File.new()
	var err = f.open(path, File.READ)
	if err != OK:
		printerr("Could not open file, error code ", err)
		return ""
		
	var letters = f.get_as_text()
	letters = letters.split("\n",false)
	
	letters = shuffleList(letters)
	f.close()

	return letters

func _input(event):
	if !event.is_pressed():
		return

	var key = event.as_text()
	if key == "PageUp": music_volume(.5)
	elif key == "PageDown": music_volume(-.5)
	
func music_volume(amount):
	var volume = AudioServer.get_bus_volume_db(music_bus) + amount
	volume = clamp(volume,-80,1)
	#print(volume)
	AudioServer.set_bus_volume_db(music_bus, volume)
	Save.save()

		
func shuffleList(list):
	var shuffledList = []
	var indexList = range(list.size())
	for i in range(list.size()):
		randomize()
		var x = randi()%indexList.size()
		shuffledList.append(list[x])
		indexList.remove(x)
		list.remove(x)
	return shuffledList
