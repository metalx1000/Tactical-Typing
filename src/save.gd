extends Node


const FILE_NAME = "user://save.json"

var save_data = {}

func get_data():
	var music_vol = AudioServer.get_bus_volume_db(Global.music_bus)
	save_data = {
		"music_vol" : music_vol
	}

func save():
	get_data()
	var file = File.new()
	file.open(FILE_NAME, File.WRITE)
	file.store_string(to_json(save_data))
	file.close()

func load():
	var file = File.new()
	if file.file_exists(FILE_NAME):
		file.open(FILE_NAME, File.READ)
		var data = parse_json(file.get_as_text())
		file.close()
		if typeof(data) == TYPE_DICTIONARY:
			save_data = data
			print("loading:" + str(save_data.music_vol))
			AudioServer.set_bus_volume_db(Global.music_bus, save_data.music_vol)
		else:
			printerr("Corrupted data!")
	else:
		printerr("No saved data!")
